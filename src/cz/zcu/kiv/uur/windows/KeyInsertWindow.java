package cz.zcu.kiv.uur.windows;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class KeyInsertWindow {
    private BorderPane rootPane;
    private final Button button;

    public KeyInsertWindow(Button button) {
        this.button = button;
        createStage();
    }

    private void createStage() {
        rootPane = new BorderPane();
        Label lbl = new Label("Stiskněte tlačítko!");
        lbl.setPadding(new Insets(50,100,50,100));
        lbl.setFont(new Font(18));
        rootPane.setCenter(lbl);

        Scene sc = new Scene(rootPane);

        Stage st = new Stage();
        st.setResizable(false);
        st.setScene(sc);
        st.setTitle("Stiskněte tlačítko!");

        sc.setOnKeyPressed(e -> {
            button.setText(e.getCode().toString());
            st.close();
        });

        st.show();
    }
}
