package cz.zcu.kiv.uur.windows;

import cz.zcu.kiv.uur.customComponents.DoubleCell;
import cz.zcu.kiv.uur.customComponents.OrderCell;
import cz.zcu.kiv.uur.customComponents.TimeCell;
import cz.zcu.kiv.uur.game.rating.Rating;
import cz.zcu.kiv.uur.game.rating.RatingList;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;


public class RatingWindow {
    private BorderPane rootPane;
    private boolean autoSort = true;
    private final int MIN_TABLE_COLUMN_SIZE = 150;

    public RatingWindow() {
        Stage stage = createStage();

        createContent();

        stage.show();
    }

    private Stage createStage() {
        rootPane = new BorderPane();
        Label lbl = new Label("Test Rating");
        rootPane.setCenter(lbl);

        Scene sc = new Scene(rootPane);

        Stage st = new Stage();
        st.setScene(sc);
        st.setTitle("Tabulka hodnocení");
        return st;
    }

    private void createContent() {
        rootPane.setCenter(createTable());
    }

    private TableView createTable() {
        TableView<Rating> table = new TableView<>();
        TableColumn<Rating, Integer> numberColumn = new TableColumn<Rating, Integer>("Pořadí");
        TableColumn<Rating, String> nameColumn = new TableColumn<Rating, String>("Jméno hráče");
        TableColumn<Rating, Long> timeColumn = new TableColumn<Rating, Long>("Čas hry");
        TableColumn<Rating, Integer> pointsColumn = new TableColumn<Rating, Integer>("Počet bodů");
        TableColumn<Rating, Double> pointsPerMinuteColumn = new TableColumn<Rating, Double>("Počet bodů za minutu");

        numberColumn.setMinWidth(40);
        numberColumn.setSortable(false);
        numberColumn.setCellFactory(column -> new OrderCell());

        nameColumn.setCellFactory(column -> new TextFieldTableCell<>());
        nameColumn.setCellValueFactory(new PropertyValueFactory<Rating, String>("playerName"));
        nameColumn.setSortable(false);
        nameColumn.setMinWidth(MIN_TABLE_COLUMN_SIZE);

        timeColumn.setCellFactory(column -> new TimeCell());
        timeColumn.setCellValueFactory(new PropertyValueFactory<Rating, Long>("gameTime"));
        timeColumn.setSortable(false);
        timeColumn.setMinWidth(MIN_TABLE_COLUMN_SIZE);

        pointsColumn.setCellFactory(column -> new TextFieldTableCell<>());
        pointsColumn.setCellValueFactory(new PropertyValueFactory<Rating, Integer>("points"));
        pointsColumn.setMinWidth(MIN_TABLE_COLUMN_SIZE);

        pointsPerMinuteColumn.setCellFactory(column -> new DoubleCell(1));
        pointsPerMinuteColumn.setCellValueFactory(new PropertyValueFactory<Rating, Double>("pointsPerMinute"));
        pointsPerMinuteColumn.setSortable(false);
        pointsPerMinuteColumn.setMinWidth(MIN_TABLE_COLUMN_SIZE);

        table.getColumns().addAll(numberColumn, nameColumn, timeColumn, pointsColumn, pointsPerMinuteColumn);
        table.setItems(RatingList.getInstance().getRatingList());
        table.setEditable(false);
        //table.setSelectionModel(null);

        table.getSortOrder().add(pointsColumn);
        pointsColumn.setSortType(TableColumn.SortType.DESCENDING);
        table.sort();

        pointsColumn.setSortable(false);

        return table;
    }
}
