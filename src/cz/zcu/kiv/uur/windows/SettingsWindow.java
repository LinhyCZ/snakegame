package cz.zcu.kiv.uur.windows;

import cz.zcu.kiv.uur.settings.ControlsGroup;
import cz.zcu.kiv.uur.game.GameGrid;
import cz.zcu.kiv.uur.settings.PlayerSettings;
import cz.zcu.kiv.uur.settings.Settings;
import cz.zcu.kiv.uur.exceptions.BadParamException;
import cz.zcu.kiv.uur.parsers.SettingsParser;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;


public class SettingsWindow {
    private final int KEY_BUTTON_WIDTH = 50;
    private final int LABEL_SIZE = 300;
    private final int INPUT_SIZE = 200;
    private final int BUTTON_SPACING = 10;
    private final Insets HBOX_INSETS = new Insets(3,10,3,10);
    private final Insets RADIO_INSETS = new Insets(0, 5, 0, 5);

    private TextField gameSpeed, intervalOfFood, areaSize, bonusProbability;
    private CheckBox generateBonusFood;
    private ToggleGroup numberOfPlayersGroup;
    private VBox playerSettingsVBox;

    private BorderPane rootPane;
    private final Stage stage;
    private final GameGrid grid;
    private Node[] playerBoxes = new Node[5];

    Settings settings = Settings.getInstance();

    public SettingsWindow(GameGrid grid) {
        this.grid = grid;

        stage = createStage();

        createContent();

        stage.show();
    }

    private Stage createStage() {
        rootPane = new BorderPane();

        Scene sc = new Scene(rootPane);

        Stage st = new Stage();
        st.setResizable(false);
        st.setScene(sc);
        st.setTitle("Nastavení");
        return st;
    }

    public void createContent() {
        HBox topHBox = new HBox();
        topHBox.setAlignment(Pos.CENTER);
        Label topLabel = new Label("Nastavení");
        topLabel.setFont(Font.font(25));
        topHBox.getChildren().addAll(topLabel);

        rootPane.setTop(topHBox);

        VBox centerPane = new VBox();
        centerPane.setAlignment(Pos.TOP_CENTER);
        rootPane.setCenter(centerPane);

        VBox centerVBox = new VBox();
        centerPane.getChildren().add(centerVBox);

        HBox gameSpeedHBox = new HBox();
        gameSpeedHBox.setAlignment(Pos.CENTER);
        gameSpeedHBox.setPadding(HBOX_INSETS);
        Label gameSpeedLabel = new Label("Rychlost hry (1-50)");
        gameSpeedLabel.setMinWidth(LABEL_SIZE);
        gameSpeed = new TextField(settings.getSpeedOfGame() + "");
        gameSpeed.setMinWidth(INPUT_SIZE);
        gameSpeedHBox.getChildren().addAll(gameSpeedLabel, gameSpeed);

        HBox areaSizeHBox = new HBox();
        areaSizeHBox.setAlignment(Pos.CENTER);
        areaSizeHBox.setPadding(HBOX_INSETS);
        Label areaSizeLabel = new Label("Velikost herního pole (15-100)");
        areaSizeLabel.setMinWidth(LABEL_SIZE);
        areaSize = new TextField(settings.getAreaSize() + "");
        areaSize.setMinWidth(INPUT_SIZE);
        areaSizeHBox.getChildren().addAll(areaSizeLabel, areaSize);

        HBox generateBonusFoodHBox = new HBox();
        generateBonusFoodHBox.setAlignment(Pos.CENTER);
        generateBonusFoodHBox.setPadding(HBOX_INSETS);
        Label generateBonusFoodLabel = new Label("Generovat bonusovou potravu");
        generateBonusFoodLabel.setMinWidth(LABEL_SIZE);
        generateBonusFood = new CheckBox();
        generateBonusFood.setSelected(settings.isGenerateBonus());
        generateBonusFood.setMinWidth(INPUT_SIZE);
        generateBonusFoodHBox.getChildren().addAll(generateBonusFoodLabel, generateBonusFood);

        HBox bonusProbabilityHBox = new HBox();
        bonusProbabilityHBox.setAlignment(Pos.CENTER);
        bonusProbabilityHBox.setPadding(HBOX_INSETS);
        Label bonusProbabilityLabel = new Label("Pravděpodobnost bonusové potravy (1-100%)");
        bonusProbabilityLabel.setMinWidth(LABEL_SIZE);
        bonusProbability = new TextField(settings.getBonusProbability() + "");
        bonusProbability.setMinWidth(INPUT_SIZE);
        bonusProbabilityHBox.getChildren().addAll(bonusProbabilityLabel, bonusProbability);

        HBox intervalOfFoodHBox = new HBox();
        intervalOfFoodHBox.setAlignment(Pos.CENTER);
        intervalOfFoodHBox.setPadding(HBOX_INSETS);
        Label intervalOfFoodLabel = new Label("Interval mizení potravy (0: nemizí)");
        intervalOfFoodLabel.setMinWidth(LABEL_SIZE);
        intervalOfFood = new TextField(settings.getIntervalOfFood() + "");
        intervalOfFood.setMinWidth(INPUT_SIZE);
        intervalOfFoodHBox.getChildren().addAll(intervalOfFoodLabel, intervalOfFood);

        HBox numberOfPlayersHBox = new HBox();
        numberOfPlayersHBox.setAlignment(Pos.CENTER);
        numberOfPlayersHBox.setPadding(HBOX_INSETS);
        Label numberOfPlayersLabel = new Label("Počet hráčů");
        numberOfPlayersLabel.setMinWidth(LABEL_SIZE);
        numberOfPlayersGroup = new ToggleGroup();

        playerSettingsVBox = new VBox();
        addPlayerSettings(playerSettingsVBox, settings.getNumberOfPlayers());

        HBox radioHBox = new HBox();
        RadioButton onePlayer = new RadioButton("1");

        if (settings.getNumberOfPlayers() == 1) {
            onePlayer.setSelected(true);
        }

        onePlayer.setToggleGroup(numberOfPlayersGroup);
        onePlayer.setPadding(RADIO_INSETS);
        onePlayer.setOnMouseClicked(e -> addPlayerSettings(playerSettingsVBox, 1));

        RadioButton twoPlayers = new RadioButton("2");

        if (settings.getNumberOfPlayers() == 2) {
            twoPlayers.setSelected(true);
        }

        twoPlayers.setToggleGroup(numberOfPlayersGroup);
        twoPlayers.setPadding(RADIO_INSETS);
        twoPlayers.setOnMouseClicked(e -> addPlayerSettings(playerSettingsVBox, 2));

        RadioButton threePlayers = new RadioButton("3");

        if (settings.getNumberOfPlayers() == 3) {
            threePlayers.setSelected(true);
        }

        threePlayers.setToggleGroup(numberOfPlayersGroup);
        threePlayers.setPadding(RADIO_INSETS);
        threePlayers.setOnMouseClicked(e -> addPlayerSettings(playerSettingsVBox, 3));

        RadioButton fourPlayers = new RadioButton("4");

        if (settings.getNumberOfPlayers() == 4) {
            fourPlayers.setSelected(true);
        }

        fourPlayers.setToggleGroup(numberOfPlayersGroup);
        fourPlayers.setPadding(RADIO_INSETS);
        fourPlayers.setOnMouseClicked(e -> addPlayerSettings(playerSettingsVBox, 4));

        RadioButton fivePlayers = new RadioButton("5");

        if (settings.getNumberOfPlayers() == 5) {
            fivePlayers.setSelected(true);
        }

        fivePlayers.setToggleGroup(numberOfPlayersGroup);
        fivePlayers.setPadding(RADIO_INSETS);
        fivePlayers.setOnMouseClicked(e -> addPlayerSettings(playerSettingsVBox, 5));

        radioHBox.getChildren().addAll(onePlayer, twoPlayers, threePlayers, fourPlayers, fivePlayers);
        radioHBox.setMinWidth(INPUT_SIZE);

        numberOfPlayersHBox.getChildren().addAll(numberOfPlayersLabel, radioHBox);

        HBox importButtonsHBox = new HBox();
        importButtonsHBox.setSpacing(BUTTON_SPACING);
        importButtonsHBox.setPadding(HBOX_INSETS);
        Button importButton = new Button("Importovat nastavení");
        importButton.setOnMouseClicked(e -> showImportDialog());
        Button exportButton = new Button("Exportovat nastavení");
        exportButton.setOnMouseClicked(e -> showExportDialog());
        importButtonsHBox.getChildren().addAll(importButton, exportButton);
        importButtonsHBox.setAlignment(Pos.TOP_CENTER);

        HBox saveButtonsHBox = new HBox();
        saveButtonsHBox.setSpacing(BUTTON_SPACING);
        saveButtonsHBox.setPadding(HBOX_INSETS);
        Button saveButton = new Button("Uložit");
        saveButton.setOnMouseClicked(e -> saveSettings());

        Button cancelButton = new Button("Zrušit");
        cancelButton.setOnAction(e -> stage.close());

        saveButtonsHBox.getChildren().addAll(saveButton, cancelButton);
        saveButtonsHBox.setAlignment(Pos.TOP_CENTER);

        centerVBox.getChildren().addAll(gameSpeedHBox, areaSizeHBox, generateBonusFoodHBox, bonusProbabilityHBox, intervalOfFoodHBox, numberOfPlayersHBox, playerSettingsVBox, importButtonsHBox, saveButtonsHBox);

        stage.sizeToScene();
    }

    public void saveSettings() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Uložení nastavení");
        alert.setHeaderText("Uložení nastavení vyresetuje hru. Chcete pokračovat?");
        alert.showAndWait()
                .filter(response -> response == ButtonType.OK)
                .ifPresent(response -> {
                    try {
                        boolean bonusFood = getBonusFood();
                        int speedOfGame = getSpeedOfGame();
                        int intervalOfFood = getIntervalOfFood();
                        int numberOfPlayers = getNumberOfPlayers();
                        int areaSize = getAreaSize();
                        int bonusProbability = getBonusProbability();


                        PlayerSettings[] playerSettings = new PlayerSettings[numberOfPlayers];

                        for (int i = 0; i < numberOfPlayers; i++) {
                            playerSettings[i] = new PlayerSettings(getPlayerName(i), getPlayerColor(i), getPlayerControls(i));
                        }

                        Settings.getInstance().proccessSettings(playerSettings, bonusFood, speedOfGame, areaSize, intervalOfFood, numberOfPlayers,  bonusProbability);

                        stage.close();
                        grid.newGame();
                    } catch (BadParamException e) {
                        showBadParamDialog(e.getMessage());
                    } catch (IOException e) {
                        showBadParamDialog("Chyba programu! Nelze uložit soubor s nastavením!");
                        System.out.println(e.getMessage());
                        stage.close();
                        grid.newGame();
                    }
                });
    }

    public String getPlayerName(int playerNumber) throws BadParamException {
        VBox playerVBox = (VBox) playerSettingsVBox.getChildren().get(playerNumber);
        HBox playerNameHBox = (HBox)playerVBox.getChildren().get(0);
        TextField playerName = (TextField)playerNameHBox.getChildren().get(1);

        if (!playerName.getText().equals("")) {
            return playerName.getText();
        } else {
            throw new BadParamException("Nezadáno uživatelské jméno!");
        }
    }

    public Color getPlayerColor(int playerNumber) {
        VBox playerVBox = (VBox) playerSettingsVBox.getChildren().get(playerNumber);
        HBox playerColorHBox = (HBox)playerVBox.getChildren().get(1);
        ColorPicker playerColor = (ColorPicker) playerColorHBox.getChildren().get(1);

        return playerColor.getValue();
    }

    public ControlsGroup getPlayerControls(int playerNumber) {
        VBox playerVBox = (VBox) playerSettingsVBox.getChildren().get(playerNumber);
        HBox playerControlsHBox = (HBox)playerVBox.getChildren().get(2);
        HBox controlsHBox = (HBox) playerControlsHBox.getChildren().get(1);
        GridPane controlsGrid = (GridPane) controlsHBox.getChildren().get(0);
        Button buttonTop = (Button)controlsGrid.getChildren().get(0);
        Button buttonDown = (Button)controlsGrid.getChildren().get(1);
        Button buttonLeft = (Button)controlsGrid.getChildren().get(2);
        Button buttonRight = (Button)controlsGrid.getChildren().get(3);

        return new ControlsGroup(KeyCode.valueOf(buttonTop.getText()), KeyCode.valueOf(buttonDown.getText()), KeyCode.valueOf(buttonLeft.getText()), KeyCode.valueOf(buttonRight.getText()));
    }

    public boolean getBonusFood() {
        return generateBonusFood.isSelected();
    }

    public int getSpeedOfGame() throws BadParamException {
        if (!gameSpeed.getText().equals("")) {
            try {
                int speed = Integer.parseInt(gameSpeed.getText());
                if (speed < 1 || speed > 50) {
                    throw new BadParamException("Rychlost hry není v zadaném intervalu!");
                } else {
                    return speed;
                }
            } catch (NumberFormatException e) {
                throw new BadParamException("Rychlost hry není číslo!");
            }
        } else {
            throw new BadParamException("Nezadána rychlost hry!");
        }
    }

    public int getNumberOfPlayers() throws BadParamException {
        RadioButton selected = (RadioButton) numberOfPlayersGroup.getSelectedToggle();
        try {
            return Integer.parseInt(selected.getText());
        } catch (Exception e) {
            throw new BadParamException("Chyba při čtení počtu hráčů!");
        }
    }

    public int getAreaSize() throws BadParamException {
        if (!areaSize.getText().equals("")) {
            try {
                int size = Integer.parseInt(areaSize.getText());
                if (size < 15 || size > 100) {
                    throw new BadParamException("Velikost herního pole není v zadaném intervalu!");
                } else {
                    return size;
                }
            } catch (NumberFormatException e) {
                throw new BadParamException("Velikost herního pole není číslo!");
            }
        } else {
            throw new BadParamException("Nezadána velikost herního pole!");
        }
    }

    public int getIntervalOfFood() throws BadParamException {
        if (!intervalOfFood.getText().equals("")) {
            try {
                int interval = Integer.parseInt(intervalOfFood.getText());
                return interval;
            } catch (NumberFormatException e) {
                throw new BadParamException("Interval jídla není číslo!");
            }
        } else {
            throw new BadParamException("Čas mizení potravy nebyl zadán");
        }
    }

    public int getBonusProbability() throws BadParamException {
        if (!bonusProbability.getText().equals("")) {
            try {
                int probability = Integer.parseInt(bonusProbability.getText());
                if (probability < 1 || probability > 100) {
                    throw new BadParamException("Pravděpodobnost bonusu není v rozsahu 1-100%!");
                } else {
                    return probability;
                }
            } catch (NumberFormatException e) {
                throw new BadParamException("Pravděpodobnost bonusu není číslo!");
            }
        } else {
            throw new BadParamException("Nezadána pravděpodobnost bonusu!");
        }
    }

    public void showBadParamDialog(String badParamMessage) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Chyba");
        alert.setHeaderText("Chyba vstupu");
        alert.setContentText(badParamMessage);

        alert.showAndWait();
    }

    private void addPlayerSettings(VBox rootVBox, int numberOfPlayers) {
        rootVBox.getChildren().clear();

        for (int i = 0; i < numberOfPlayers; i++) {
            if (playerBoxes[i] == null) {
                playerBoxes[i] = createPlayerVBox(i);
                rootVBox.getChildren().add(playerBoxes[i]);
            } else {
                rootVBox.getChildren().add(playerBoxes[i]);
            }
        }
        stage.sizeToScene();
    }

    private Node createPlayerVBox(int numberOfPlayer) {
        VBox playerVBox = new VBox();


        if (numberOfPlayer % 2 == 0) {
            playerVBox.setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));
        }

        HBox playerNameHBox = new HBox();
        playerNameHBox.setAlignment(Pos.CENTER);
        playerNameHBox.setPadding(HBOX_INSETS);
        Label playerNameLabel = new Label("Jméno hráče " + (numberOfPlayer + 1));
        playerNameLabel.setMinWidth(LABEL_SIZE);
        TextField playerName = new TextField(settings.getPlayerName(numberOfPlayer));
        playerName.setMinWidth(INPUT_SIZE);
        playerNameHBox.getChildren().addAll(playerNameLabel, playerName);

        HBox playerColorHBox = new HBox();
        playerColorHBox.setAlignment(Pos.CENTER);
        playerColorHBox.setPadding(HBOX_INSETS);
        Label playerColorLabel = new Label("Barva hada");
        playerColorLabel.setMinWidth(LABEL_SIZE);
        ColorPicker playerColor = new ColorPicker(settings.getPlayerColor(numberOfPlayer));
        playerColor.setMinWidth(INPUT_SIZE);
        playerColorHBox.getChildren().addAll(playerColorLabel, playerColor);

        HBox playerControlsHBox = new HBox();
        playerControlsHBox.setAlignment(Pos.CENTER);
        playerControlsHBox.setPadding(HBOX_INSETS);
        Label playerControlsLabel = new Label("Nastavení ovládání");
        playerControlsLabel.setMinWidth(LABEL_SIZE);

        HBox controlsHBox = new HBox();
        controlsHBox.setAlignment(Pos.CENTER);
        controlsHBox.setMinWidth(INPUT_SIZE);

        GridPane controls = new GridPane();

        Button forward = new Button(settings.getPlayerControls(numberOfPlayer).getKeyUp().toString());
        forward.setOnMouseClicked(e -> new KeyInsertWindow(forward));

        Button backward = new Button(settings.getPlayerControls(numberOfPlayer).getKeyDown().toString());
        backward.setOnMouseClicked(e -> new KeyInsertWindow(backward));

        Button left = new Button(settings.getPlayerControls(numberOfPlayer).getKeyLeft().toString());
        left.setOnMouseClicked(e -> new KeyInsertWindow(left));

        Button right = new Button(settings.getPlayerControls(numberOfPlayer).getKeyRight().toString());
        right.setOnMouseClicked(e -> new KeyInsertWindow(right));

        forward.setMinWidth(KEY_BUTTON_WIDTH);
        backward.setMinWidth(KEY_BUTTON_WIDTH);
        left.setMinWidth(KEY_BUTTON_WIDTH);
        right.setMinWidth(KEY_BUTTON_WIDTH);

        controls.add(forward, 1, 0);
        controls.add(backward, 1, 1);
        controls.add(left, 0, 1);
        controls.add(right, 2, 1);

        controlsHBox.getChildren().addAll(controls);
        playerControlsHBox.getChildren().addAll(playerControlsLabel, controlsHBox);

        playerVBox.getChildren().addAll(playerNameHBox, playerColorHBox, playerControlsHBox);

        return playerVBox;
    }

    public void showImportDialog() {
        FileChooser fc = new FileChooser();
        FileChooser.ExtensionFilter filter = new FileChooser.ExtensionFilter("Snake Settings files (*.snakesettings)", "*.snakesettings");
        fc.getExtensionFilters().add(filter);

        File file = fc.showOpenDialog(stage);
        if (file != null) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Import");
            alert.setHeaderText("Import");
            alert.setContentText("Opravdu chcete importovat nastavení? Toto přepíše vaše předchozí nastavení a resetuje hru.");
            alert.showAndWait()
                .filter(response -> response == ButtonType.OK)
                .ifPresent(response -> {
                    try {
                        Settings settings = SettingsParser.parseSettingsFromFile(file);
                        Settings.getInstance().importSettings(settings);

                        this.createContent();
                        grid.newGame();

                        Alert alert2 = new Alert(Alert.AlertType.INFORMATION);
                        alert2.setTitle("Import");
                        alert2.setHeaderText("Importováno!");
                        alert2.setContentText("Nastavení úspěšně importováno!");
                        alert2.showAndWait();
                    } catch (FileNotFoundException ex) {
                        Alert alert2 = new Alert(Alert.AlertType.ERROR);
                        alert2.setTitle("Import");
                        alert2.setHeaderText("Nelze importovat!");
                        alert2.setContentText("Chyba: " + ex.toString());
                        alert2.showAndWait();
                    }
                });
        }
    }

    public void showExportDialog() {
        FileChooser fc = new FileChooser();
        FileChooser.ExtensionFilter filter = new FileChooser.ExtensionFilter("Snake Settings files (*.snakesettings)", "*.snakesettings");
        fc.getExtensionFilters().add(filter);

        File file = fc.showSaveDialog(stage);

        if (file != null) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Export");
            alert.setHeaderText("Export");
            alert.setContentText("Opravdu chcete exportovat nastavení? Toto uloží aktuální nastavení a resetuje hru!.");
            alert.showAndWait()
                    .filter(response -> response == ButtonType.OK)
                    .ifPresent(response -> {
                        this.saveSettings();
                        grid.newGame();
                        try {
                            SettingsParser.settingsToFile(file);

                            Alert alert2 = new Alert(Alert.AlertType.INFORMATION);
                            alert2.setTitle("Export");
                            alert2.setHeaderText("Exportováno!");
                            alert2.setContentText("Cesta k souboru: " + file.getAbsolutePath());
                            alert2.showAndWait();
                        } catch (IOException ex) {
                            Alert alert2 = new Alert(Alert.AlertType.ERROR);
                            alert2.setTitle("Export");
                            alert2.setHeaderText("Nelze exportovat!");
                            alert2.setContentText("Chyba: " + ex.toString());
                            alert2.showAndWait();
                        }
                    });
        }
    }
}
