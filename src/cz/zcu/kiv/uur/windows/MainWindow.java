package cz.zcu.kiv.uur.windows;

import cz.zcu.kiv.uur.game.GameGrid;
import cz.zcu.kiv.uur.parsers.RatingsParser;
import cz.zcu.kiv.uur.parsers.SettingsParser;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class MainWindow {
    private static MainWindow instance;
    private Stage stage;
    private Scene scene;
    private GameGrid game;

    private final int GUI_HEIGHT = 88;
    private final int GUI_WIDTH = 17;

    private Button pauseButton, darkModeButton;
    private Label score, gameTime;

    private MainWindow() {
        SettingsParser.loadDefaultSettings();
        RatingsParser.loadDefaultRatings();
    }

    public static MainWindow getInstance() {
        if (instance == null) {
            instance = new MainWindow();
        }

        return MainWindow.instance;
    }

    public void setStage(Stage stage) {
        this.stage = stage;

        drawComponents();
    }

    private void drawComponents() {
        BorderPane rootPane = new BorderPane();
        scene = new Scene(rootPane);

        MenuBar menuBar = createMenuBar();
        rootPane.setTop(menuBar);
        rootPane.setCenter(createGridWithTopBar());

        updateStageSettings(scene);

        this.stage.show();
    }

    private void updateStageSettings(Scene sc) {
        this.stage.setScene(sc);
        this.stage.setTitle("Snake game");

        this.stage.setMinWidth(1000);
        this.stage.setMinHeight(800);

        this.stage.setOnCloseRequest(e -> System.exit(0));
    }

    private Node createGridWithTopBar() {
        BorderPane gridAndBarPane = new BorderPane();
        HBox topBar = new HBox();
        topBar.setAlignment(Pos.CENTER);

        topBar.setBackground(new Background(new BackgroundFill(Color.LIGHTGRAY, CornerRadii.EMPTY, new Insets(0))));


        Button newGameButton = new Button("Nová hra");
        newGameButton.setOnMouseClicked(event -> game.newGame());
        pauseButton = new Button("Začít hrát!");
        pauseButton.setOnMouseClicked(event -> this.pauseEvent(event));
        darkModeButton = new Button("Dark mode");
        darkModeButton.setOnMouseClicked(event -> this.darkModeEvent(event));

        score = new Label("");
        score.setPadding(new Insets(0,20,0,0));
        gameTime = new Label("Čas hry: 0s");
        gameTime.setPadding(new Insets(0,20,0,0));
        Region spacer = new Region();
        HBox.setHgrow(spacer, Priority.ALWAYS);


        topBar.getChildren().addAll(newGameButton, pauseButton, darkModeButton, spacer, gameTime, score);


        game = new GameGrid();
        game.newGame();
        gridAndBarPane.setTop(topBar);
        gridAndBarPane.setCenter(game);

        return gridAndBarPane;
    }

    private MenuBar createMenuBar() {
        MenuBar bar = new MenuBar();

        Label labelSettings = new Label("Nastavení");
        labelSettings.setPadding(new Insets(0,0,0,0));
        labelSettings.setOnMouseClicked(mouseEvent -> new SettingsWindow(this.game));

        Label labelRating = new Label("Tabulka hodnocení");
        labelRating.setPadding(new Insets(0,0,0,0));
        labelRating.setOnMouseClicked(mouseEvent -> new RatingWindow());


        Menu menuRating = new Menu("", labelRating);
        Menu menuSettings = new Menu("", labelSettings);

        bar.getMenus().addAll(menuSettings, menuRating);

        return bar;
    }

    private void pauseEvent(MouseEvent event) {
        Button evButton = (Button) event.getSource();
        if (evButton.getText().equals("Pauza")) {
            game.pauseTimer();
            evButton.setText("Pokračovat");
        } else {
            game.startTimer();
            evButton.setText("Pauza");
        }
    }

    private void darkModeEvent(MouseEvent event) {
        Button evButton = (Button) event.getSource();
        if (evButton.getText().equals("Dark mode")) {
            game.setDarkMode(true);
            evButton.setText("Light mode");
        } else {
            game.setDarkMode(false);
            evButton.setText("Dark mode");
        }
    }

    public void resetWindow() {
        this.pauseButton.setText("Začít hrát!");
    }

    public Scene getScene() {
        return scene;
    }

    public void setScoreLabel(String newScoreLabelText) {
        score.setText(newScoreLabelText);
    }

    public void setTimeLabel(String labelText) {

        gameTime.setText(labelText);
    }
}
