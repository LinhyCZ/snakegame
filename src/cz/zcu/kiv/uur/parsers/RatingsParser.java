package cz.zcu.kiv.uur.parsers;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import cz.zcu.kiv.uur.game.rating.RatingList;
import org.hildan.fxgson.FxGson;

import java.io.*;

public class RatingsParser {
    private static final String DEFAULT_RATINGS_FILENAME = "data/settings.snakerating";
    private static final Gson gson = FxGson.create();

    public static void loadDefaultRatings() {
        try {
            RatingList list = parseRatingsFromFile(DEFAULT_RATINGS_FILENAME);
            if (list != null) {
                RatingList.setInstance(list);
            } else {
                System.out.println("Nelze načíst soubor s výsledky!");
            }
        } catch (Exception e) {
            System.out.println("Nelze načíst soubor s výsledky!");
        }
    }

    public static RatingList parseRatingsFromFile(String filename) throws FileNotFoundException {
        JsonReader reader = new JsonReader(new FileReader(filename));
        RatingList fromJson = gson.fromJson(reader, RatingList.class);
        return fromJson;
    }

    public static String ratingsToJson() {
        String json = gson.toJson(RatingList.getInstance());
        return json;
    }

    public static void ratingsToFile(String filename) throws IOException {
        File file = new File(filename);
        file.getParentFile().mkdirs();
        FileWriter fw = new FileWriter(file);
        fw.write(ratingsToJson());
        fw.close();
    }

    public static void saveDefaultRatings() throws IOException {
        ratingsToFile(DEFAULT_RATINGS_FILENAME);
    }
}
