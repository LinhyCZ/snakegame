package cz.zcu.kiv.uur.parsers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;
import cz.zcu.kiv.uur.settings.Settings;

import java.io.*;

public class SettingsParser {
    private static final String DEFAULT_SETTINGS_FILENAME = "data"+ File.separator + "settings.snakesettings";
    private static final Gson gsonOut = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
    private static final Gson gsonIn = new Gson();

    public static void loadDefaultSettings() {
        try {
            Settings.getInstance().importSettings(parseSettingsFromFile(DEFAULT_SETTINGS_FILENAME));
        } catch (Exception e) {
            Settings.getInstance().loadDefaults();
        }
    }

    public static Settings parseSettingsFromFile(File file) throws FileNotFoundException {
        JsonReader reader = new JsonReader(new FileReader(file));
        Settings fromJson = gsonIn.fromJson(reader, Settings.class);
        return fromJson;
    }


    public static Settings parseSettingsFromFile(String filename) throws FileNotFoundException {
        JsonReader reader = new JsonReader(new FileReader(filename));
        Settings fromJson = gsonIn.fromJson(reader, Settings.class);
        return fromJson;
    }

    public static String settingsToJson() {
        String json = gsonOut.toJson(Settings.getInstance());
        return json;
    }

    public static void settingsToFile(File file) throws IOException {
        file.getParentFile().mkdirs();
        FileWriter fw = new FileWriter(file);
        fw.write(settingsToJson());
        fw.close();
    }

    public static void settingsToFile(String filename) throws IOException {
        File file = new File(filename);
        settingsToFile(file);
    }

    public static void saveDefaultSettings() throws IOException {
        settingsToFile(DEFAULT_SETTINGS_FILENAME);
    }

    private SettingsParser() {

    }
}
