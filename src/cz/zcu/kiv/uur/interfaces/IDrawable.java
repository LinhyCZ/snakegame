package cz.zcu.kiv.uur.interfaces;

import cz.zcu.kiv.uur.exceptions.BadPointCoordinateException;
import cz.zcu.kiv.uur.game.drawer.GridDrawer;

public interface IDrawable {
    public void draw(GridDrawer drawer) throws BadPointCoordinateException;
}
