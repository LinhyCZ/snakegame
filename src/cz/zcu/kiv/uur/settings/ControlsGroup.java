package cz.zcu.kiv.uur.settings;

import com.google.gson.annotations.Expose;
import javafx.scene.input.KeyCode;

public class ControlsGroup {
    @Expose
    private final KeyCode keyUp;

    @Expose
    private final KeyCode keyDown;

    @Expose
    private final KeyCode keyLeft;

    @Expose
    private final KeyCode keyRight;

    public ControlsGroup(KeyCode keyUp, KeyCode keyDown, KeyCode keyLeft, KeyCode keyRight) {
        this.keyUp = keyUp;
        this.keyDown = keyDown;
        this.keyLeft = keyLeft;
        this.keyRight = keyRight;
    }

    public KeyCode getKeyUp() {
        return keyUp;
    }

    public KeyCode getKeyDown() {
        return keyDown;
    }

    public KeyCode getKeyLeft() {
        return keyLeft;
    }

    public KeyCode getKeyRight() {
        return keyRight;
    }
}
