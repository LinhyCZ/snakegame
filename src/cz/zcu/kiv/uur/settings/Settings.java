package cz.zcu.kiv.uur.settings;

import com.google.gson.annotations.Expose;
import cz.zcu.kiv.uur.parsers.SettingsParser;
import javafx.scene.input.KeyCode;
import javafx.scene.paint.Color;

import java.io.IOException;

public class Settings {
    private static Settings instance;

    private final String PLAYER_DEFAULT_NAME = "Player";
    private final Color PLAYER_DEFAULT_COLOR = Color.YELLOWGREEN;
    private final ControlsGroup[] PLAYER_DEFAULT_CONTROLS = new ControlsGroup[] {
            new ControlsGroup(KeyCode.W, KeyCode.S, KeyCode.A, KeyCode.D),
            new ControlsGroup(KeyCode.T, KeyCode.G, KeyCode.F, KeyCode.H),
            new ControlsGroup(KeyCode.I, KeyCode.K, KeyCode.J, KeyCode.L),
            new ControlsGroup(KeyCode.UP, KeyCode.DOWN, KeyCode.LEFT, KeyCode.RIGHT),
            new ControlsGroup(KeyCode.NUMPAD8, KeyCode.NUMPAD5, KeyCode.NUMPAD4, KeyCode.NUMPAD6)
    };
    private final int INTERVAL_OF_FOOD_DEFAULT = 0;
    private final boolean GENERATE_BONUS_DEFAULT = false;
    private final int SPEED_OF_GAME_DEFAULT = 5;
    private final int NUMBER_OF_PLAYERS_DEFAULT = 1;
    private final int AREA_SIZE_DEFAULT = 30;
    private final int BONUS_PROBABILITY_DEFAULT = 5;
    private final PlayerSettings DEFAULT_PLAYER_SETTINGS = new PlayerSettings("Player", Color.YELLOWGREEN, new ControlsGroup(KeyCode.W, KeyCode.S, KeyCode.A, KeyCode.D));

    @Expose
    private int speedOfGame;

    @Expose
    private boolean generateBonus;

    @Expose
    private int bonusProbability;

    @Expose
    private int intervalOfFood;

    @Expose
    private int areaSize;

    @Expose
    private int numberOfPlayers;

    @Expose
    private PlayerSettings[] playerSettings = new PlayerSettings[5];


    public int getAreaSize() {
        if (this.areaSize == 0) {
            return AREA_SIZE_DEFAULT;
        }
        return areaSize;
    }

    public int getSpeedOfGame() {
        if (this.speedOfGame == 0) {
            return SPEED_OF_GAME_DEFAULT;
        }
        return speedOfGame;
    }

    public boolean isGenerateBonus() {
        return generateBonus;
    }

    public int getBonusProbability() {
        if (this.bonusProbability == 0) {
            return BONUS_PROBABILITY_DEFAULT;
        }

        return bonusProbability;
    }

    public int getIntervalOfFood() {
        return intervalOfFood;
    }

    public int getNumberOfPlayers() {
        if (numberOfPlayers == 0) {
            return NUMBER_OF_PLAYERS_DEFAULT;
        }
        return numberOfPlayers;
    }

    public PlayerSettings getPlayerSettings(int index) {
        return playerSettings[index];
    }

    public String getPlayerName(int index) {
        try {
            return playerSettings[index].getPlayerName();
        } catch (Exception e) {
            return PLAYER_DEFAULT_NAME;
        }
    }

    public Color getPlayerColor(int index) {
        try {
            Color c = playerSettings[index].getSnakeColor();
            if (c != null) {
                return c;
            } else {
                return PLAYER_DEFAULT_COLOR;
            }
        } catch (Exception e) {
            return PLAYER_DEFAULT_COLOR;
        }
    }

    public ControlsGroup getPlayerControls(int index) {
        try {
            ControlsGroup c = playerSettings[index].getSnakeControls();
            if (c != null) {
                return c;
            } else {
                return PLAYER_DEFAULT_CONTROLS[index];
            }
        } catch (Exception e) {
            return PLAYER_DEFAULT_CONTROLS[index];
        }
    }

    private Settings() {

    }

    public static Settings getInstance() {
        if (instance == null) {
            instance = new Settings();
        }

        return instance;
    }

    public void importSettings(Settings settings) {
        this.numberOfPlayers = settings.numberOfPlayers;
        this.intervalOfFood = settings.intervalOfFood;
        this.generateBonus = settings.generateBonus;
        this.speedOfGame = settings.speedOfGame;
        this.playerSettings = settings.playerSettings;
        this.areaSize = settings.areaSize;
        this.bonusProbability = settings.bonusProbability;
    }

    public void loadDefaults() {
        this.playerSettings[0] = DEFAULT_PLAYER_SETTINGS;
        this.intervalOfFood = INTERVAL_OF_FOOD_DEFAULT;
        this.generateBonus = GENERATE_BONUS_DEFAULT;
        this.speedOfGame = SPEED_OF_GAME_DEFAULT;
        this.numberOfPlayers = NUMBER_OF_PLAYERS_DEFAULT;
        this.areaSize = AREA_SIZE_DEFAULT;
        this.bonusProbability = BONUS_PROBABILITY_DEFAULT;
    }

    public void proccessSettings(PlayerSettings[] playerSettings, boolean bonusFood, int speedOfGame, int areaSize, int intervalOfFood, int numberOfPlayers, int bonusProbability) throws IOException {
        this.playerSettings = playerSettings;
        this.generateBonus = bonusFood;
        this.speedOfGame = speedOfGame;
        this.intervalOfFood = intervalOfFood;
        this.numberOfPlayers = numberOfPlayers;
        this.areaSize = areaSize;
        this.bonusProbability = bonusProbability;

        SettingsParser.saveDefaultSettings();
    }
}
