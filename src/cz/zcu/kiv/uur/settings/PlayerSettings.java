package cz.zcu.kiv.uur.settings;

import com.google.gson.annotations.Expose;
import javafx.scene.paint.Color;

public class PlayerSettings {
    @Expose
    private final String playerName;

    private Color snakeColor;

    @Expose
    private final String snakeColorHex;
    @Expose
    private final ControlsGroup snakeControls;

    public PlayerSettings(String playerName, Color color, ControlsGroup controlsGroup) {
        this.playerName = playerName;
        this.snakeColor = color;
        this.snakeColorHex = "0x" + Integer.toHexString(color.hashCode()).substring(0, 6).toUpperCase();
        this.snakeControls = controlsGroup;
    }

    public String getPlayerName() {
        return playerName;
    }

    public Color getSnakeColor() {
        if (snakeColor == null) {
            snakeColor = Color.web(snakeColorHex);
        }
        return snakeColor;
    }

    public ControlsGroup getSnakeControls() {
        return snakeControls;
    }
}
