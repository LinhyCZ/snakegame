package cz.zcu.kiv.uur.exceptions;

import cz.zcu.kiv.uur.game.entities.Point;

public class BadPointCoordinateException extends Exception {
    public BadPointCoordinateException() {
        this("Point out of bounds!");
    }

    public BadPointCoordinateException(Point point) {
        this("Point out of bounds! [" + point.getX() + ", " + point.getY() + "]");
    }

    public BadPointCoordinateException(double x, double y) {
        this("Point out of bounds! [" + x + ", " + y + "]");
    }

    public BadPointCoordinateException(String message) {
        super(message);
    }
}
