package cz.zcu.kiv.uur.exceptions;

public class BadParamException extends Exception {
    public BadParamException(String message) {
        super(message);
    }
}
