package cz.zcu.kiv.uur;

import cz.zcu.kiv.uur.windows.MainWindow;
import javafx.application.Application;
import javafx.stage.Stage;

public class MainFX extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        MainWindow window = MainWindow.getInstance();
        window.setStage(stage);
    }
}
