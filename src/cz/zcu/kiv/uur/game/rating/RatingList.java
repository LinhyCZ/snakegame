package cz.zcu.kiv.uur.game.rating;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class RatingList {
    private static RatingList instance;
    private ObservableList<Rating> list = FXCollections.observableArrayList();

    private RatingList() {

    }

    public static RatingList getInstance() {
        if (instance == null) {
            instance = new RatingList();
        }

        return instance;
    }

    public static void setInstance(RatingList list) {
        RatingList.instance = list;
    }

    public ObservableList<Rating> getRatingList() {
        return list;
    }

    public void addRating(Rating... ratings) {
        this.list.addAll(ratings);
    }
}
