package cz.zcu.kiv.uur.game.rating;

import javafx.beans.property.*;

public class Rating {
    private final StringProperty playerName;
    private final LongProperty gameTime;
    private final IntegerProperty points;
    private final DoubleProperty pointsPerMinute;

    public Rating(String playerName, long gameTime, int points) {
        this.playerName = new SimpleStringProperty(playerName);
        this.gameTime = new SimpleLongProperty(gameTime);
        this.points = new SimpleIntegerProperty(points);
        this.pointsPerMinute = new SimpleDoubleProperty(getPointsPerMinute(points, gameTime));
    }

    private double getPointsPerMinute(int points, long gameTime) {
        if (points == 0) {
            return 0;
        }
        return ((double) points / ((double) gameTime / 60));
    }

    public String getPlayerName() {
        return playerName.get();
    }

    public StringProperty playerNameProperty() {
        return playerName;
    }

    public Long getGameTime() {
        return gameTime.get();
    }

    public LongProperty gameTimeProperty() {
        return gameTime;
    }

    public int getPoints() {
        return points.get();
    }

    public IntegerProperty pointsProperty() {
        return points;
    }

    public double getPointsPerMinute() {
        return pointsPerMinute.get();
    }

    public DoubleProperty pointsPerMinuteProperty() {
        return pointsPerMinute;
    }
}
