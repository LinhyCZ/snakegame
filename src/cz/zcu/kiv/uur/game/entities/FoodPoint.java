package cz.zcu.kiv.uur.game.entities;

import cz.zcu.kiv.uur.exceptions.BadPointCoordinateException;
import cz.zcu.kiv.uur.game.GameGrid;
import cz.zcu.kiv.uur.game.drawer.GridDrawer;
import cz.zcu.kiv.uur.interfaces.IDrawable;
import cz.zcu.kiv.uur.settings.Settings;
import javafx.scene.paint.Color;

import java.util.List;
import java.util.Random;

public class FoodPoint implements IDrawable {
    private final Color COLOR_BONUS_FOOD = Color.GOLD;
    private final Color COLOR_FOOD = Color.GREEN;
    private final int SCORE_VALUE = 1;
    private final int BONUS_SCORE_VALUE = 5;

    private Point point;
    private boolean bonus;
    private long timerRunOut;
    private GameGrid grid;

    public FoodPoint(GameGrid grid) {
        this.point = generateRandomPointFromGrid(grid);
        this.timerRunOut = getTimerRunOut();
        this.bonus = getBonus();
        this.grid = grid;
    }

    public Point getPoint() {
        return point;
    }

    public boolean isBonus() {
        return bonus;
    }

    private Point generateRandomPointFromGrid(GameGrid grid) {
        Random rnd = new Random();
        List<Point> points = grid.getFreeSpaces();
        return points.get(rnd.nextInt(points.size()));
    }

    private long getTimerRunOut() {
        Settings settings = Settings.getInstance();
        int interval = settings.getIntervalOfFood();
        if (interval == 0) {
            return 0;
        } else {
            return interval + grid.getCurrentTick();
        }
    }

    private static boolean getBonus() {
        if(Settings.getInstance().isGenerateBonus()) {
            Random rnd = new Random();
            int probability = Settings.getInstance().getBonusProbability();
            int rndValue = rnd.nextInt(101);
            return rndValue <= probability;
        }

        return false;
    }

    public boolean checkTimerRunOut(long currentTick) {
        if (currentTick < timerRunOut || timerRunOut == 0) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void draw(GridDrawer drawer) throws BadPointCoordinateException {
        if (this.isBonus()) {
            drawer.drawCircle(this.getPoint(), COLOR_BONUS_FOOD);
        } else {
            drawer.drawCircle(this.getPoint(), COLOR_FOOD);
        }
    }

    public int getScore() {
        if (this.isBonus()) {
            return BONUS_SCORE_VALUE;
        } else {
            return SCORE_VALUE;
        }
    }
}
