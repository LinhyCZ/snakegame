package cz.zcu.kiv.uur.game.entities;

public class Point {
    private final int x;
    private final int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    public boolean equals(Object object) {
        if (object == this) {
            return true;
        }
        if (object instanceof Point) {
            Point p1 = (Point) object;
            if (p1.getX() == this.getX() && p1.getY() == this.getY()) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return "[" + getX() + ":" + getY() + "]";
    }
}
