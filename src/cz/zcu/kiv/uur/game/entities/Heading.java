package cz.zcu.kiv.uur.game.entities;

public enum Heading {
    NORTH, SOUTH, WEST, EAST
}
