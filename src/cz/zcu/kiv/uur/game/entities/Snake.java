package cz.zcu.kiv.uur.game.entities;

import cz.zcu.kiv.uur.exceptions.BadPointCoordinateException;
import cz.zcu.kiv.uur.game.GameGrid;
import cz.zcu.kiv.uur.game.drawer.GridDrawer;
import cz.zcu.kiv.uur.interfaces.IDrawable;
import cz.zcu.kiv.uur.settings.PlayerSettings;
import cz.zcu.kiv.uur.settings.Settings;

import java.util.*;

public class Snake implements IDrawable {
    private final int DEFAULT_SIZE = 5;
    private final int DEFAULT_POS_X = 2;
    private final int DEFAULT_POS_Y = Settings.getInstance().getAreaSize() / 2;
    private final int Y_OFFSET = 2;

    private final Heading DEFAULT_HEADING = Heading.EAST;

    private Heading lastHeading = DEFAULT_HEADING;
    private Heading nextHeading = DEFAULT_HEADING;

    private final PlayerSettings settings;

    private final Deque<Point> snakePoints = new LinkedList<>();
    private final GameGrid grid;

    private boolean isDead = false;
    private int score = 0;
    private int numberOfTicks = 0;

    public Snake(GameGrid grid, PlayerSettings settings) {
        this(0, grid, settings);
    }

    public Snake(int yOffsetMultiplier, GameGrid grid, PlayerSettings settings) {
        this.grid = grid;
        this.settings = settings;
        generateStartSnakePoints(Y_OFFSET * yOffsetMultiplier);
        bindControls();
    }

    public void generateStartSnakePoints(int yOffset) {
        for (int i = 0; i < DEFAULT_SIZE; i++) {
            snakePoints.add(new Point(DEFAULT_POS_X + i, DEFAULT_POS_Y + yOffset));
        }
    }

    public void setNextHeading(Heading heading) {
        this.nextHeading = heading;
    }

    public void move() {
        move(this.nextHeading);
    }

    public boolean isColided(int x, int y) {
        return !this.grid.getFreeSpaces().contains(new Point(x, y));
    }

    public boolean hitsFood(int x, int y) {
        return this.grid.getFood().getPoint().getX() == x && this.grid.getFood().getPoint().getY() == y;
    }

    private void move(Heading heading) {
        if (!isDead) {
            numberOfTicks++;
            if (validateHeading(heading)) {
                this.lastHeading = heading;
                Point snakeHead = snakePoints.getLast();
                switch (heading) {
                    case NORTH:
                        addPoint(snakeHead.getX(), snakeHead.getY() - 1);
                        break;
                    case SOUTH:
                        addPoint(snakeHead.getX(), snakeHead.getY() + 1);
                        break;
                    case EAST:
                        addPoint(snakeHead.getX() + 1, snakeHead.getY());
                        break;
                    case WEST:
                        addPoint(snakeHead.getX() - 1, snakeHead.getY());
                        break;
                }
            } else {
                move(lastHeading);
            }
        }
    }

    private boolean validateHeading(Heading heading) {
        if (heading == Heading.SOUTH && lastHeading == Heading.NORTH) {
            return false;
        }

        if (heading == Heading.NORTH && lastHeading == Heading.SOUTH) {
            return false;
        }

        if (heading == Heading.WEST && lastHeading == Heading.EAST) {
            return false;
        }

        if (heading == Heading.EAST && lastHeading == Heading.WEST) {
            return false;
        }

        return true;
    }

    private void addPoint(int x, int y) {
        int areaSize = Settings.getInstance().getAreaSize();
        if (x >= areaSize) x = 0;
        if (y >= areaSize) y = 0;
        if (x < 0) x = areaSize - 1;
        if (y < 0) y = areaSize - 1;

        if (isColided(x, y) && !hitsFood(x, y)) {
            this.snakePoints.clear();
            isDead = true;
            unbindControls();
        } else {
            if (!hitsFood(x, y)) {
                snakePoints.remove();
            } else {
                score += grid.getFood().getScore();
                grid.updateScoreLabel();
                grid.newFood();
            }
            snakePoints.add(new Point(x, y));
        }
    }

    @Override
    public void draw(GridDrawer drawer) throws BadPointCoordinateException {
        for (Point point : this.snakePoints) {
            drawer.drawPoint(point, settings.getSnakeColor());
        }
    }

    public List<Point> getPoints() {
        return (LinkedList) this.snakePoints;
    }

    public void bindControls() {
        grid.bindKey(settings.getSnakeControls().getKeyUp(), () -> {this.setNextHeading(Heading.NORTH);});
        grid.bindKey(settings.getSnakeControls().getKeyDown(), () -> {this.setNextHeading(Heading.SOUTH);});
        grid.bindKey(settings.getSnakeControls().getKeyLeft(), () -> {this.setNextHeading(Heading.WEST);});
        grid.bindKey(settings.getSnakeControls().getKeyRight(), () -> {this.setNextHeading(Heading.EAST);});
    }

    public void unbindControls() {
        grid.unbindKey(settings.getSnakeControls().getKeyUp());
        grid.unbindKey(settings.getSnakeControls().getKeyDown());
        grid.unbindKey(settings.getSnakeControls().getKeyLeft());
        grid.unbindKey(settings.getSnakeControls().getKeyRight());
    }

    public boolean isAlive() {
        return !isDead;
    }

    public int getScore() {
        return score;
    }

    public String getName() {
        return settings.getPlayerName();
    }

    public long getTicks() {
        return this.numberOfTicks;
    }
}
