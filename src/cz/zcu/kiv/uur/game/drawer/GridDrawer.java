package cz.zcu.kiv.uur.game.drawer;

import cz.zcu.kiv.uur.customComponents.CanvasPane;
import cz.zcu.kiv.uur.exceptions.BadPointCoordinateException;
import cz.zcu.kiv.uur.game.GameGrid;
import cz.zcu.kiv.uur.game.entities.FoodPoint;
import cz.zcu.kiv.uur.game.entities.Point;
import cz.zcu.kiv.uur.game.entities.Snake;
import cz.zcu.kiv.uur.interfaces.IDrawable;
import cz.zcu.kiv.uur.settings.Settings;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import java.util.List;

public class GridDrawer {
    private final GameGrid grid;
    private final Settings settings = Settings.getInstance();
    private final CanvasPane canvasPane;
    private final GraphicsContext gc;

    private final Color DEFAULT_COLOR = Color.BLACK;

    public GridDrawer(GameGrid grid) {
        this.grid = grid;
        this.canvasPane = grid.getCanvasPane();
        this.gc = canvasPane.getGraphicsContext2D();
    }

    public void drawPoint(Point point) throws BadPointCoordinateException {
        drawPoint(point.getX(), point.getY(), DEFAULT_COLOR);
    }

    public void drawPoint(Point point, Color color) throws BadPointCoordinateException {
        drawPoint(point.getX(), point.getY(), color);
    }

    public void drawPoint(int x, int y, Color color) throws BadPointCoordinateException {
        if (x > settings.getAreaSize() - 1) throw new BadPointCoordinateException(x, y);
        if (x < 0) throw new BadPointCoordinateException(x, y);
        if (y > settings.getAreaSize() - 1) throw new BadPointCoordinateException(x, y);
        if (y < 0) throw new BadPointCoordinateException(x, y);

        double squareSize = calculateSquareSize();
        gc.setFill(color);
        gc.fillRect(x * squareSize, y * squareSize, squareSize, squareSize);
    }

    public void drawCircle(Point point, Color color) throws BadPointCoordinateException {
        drawCircle(point.getX(), point.getY(), color);
    }

    public void drawCircle(int x, int y, Color color) throws BadPointCoordinateException {
        if (x > settings.getAreaSize() - 1) throw new BadPointCoordinateException(x, y);
        if (x < 0) throw new BadPointCoordinateException(x, y);
        if (y > settings.getAreaSize() - 1) throw new BadPointCoordinateException(x, y);
        if (y < 0) throw new BadPointCoordinateException(x, y);

        double squareSize = calculateSquareSize();
        gc.setFill(color);
        gc.fillOval(x * squareSize, y * squareSize, squareSize, squareSize);

    }

    public double calculateSquareSize() {
        return canvasPane.getCanvasSize() / settings.getAreaSize();
    }

    public void drawDrawable(IDrawable drawable) throws BadPointCoordinateException {
        drawable.draw(this);
    }

    public void drawGrid() {
        erase();

        try {
            List<Snake> snakes = grid.getSnakes();
            FoodPoint fp = grid.getFood();

            for (Snake snake : snakes) {
                drawDrawable(snake);
            }

            if (fp != null) {
                drawDrawable(fp);
            }

        } catch (BadPointCoordinateException e) {
            e.printStackTrace();
        }
    }

    private void erase() {
        gc.setFill(canvasPane.getBackgroundColor());
        gc.fillRect(0, 0, canvasPane.getCanvasSize(), canvasPane.getCanvasSize());
    }
}
