package cz.zcu.kiv.uur.game.timer;

import cz.zcu.kiv.uur.game.GameGrid;
import cz.zcu.kiv.uur.settings.Settings;

import java.util.Timer;
import java.util.TimerTask;

public class GridTimer extends Timer {
    private final GameGrid grid;
    private final TimerTask task;
    private int delay;

    private boolean started = false;
    private boolean paused = false;

    private long currentTick = 0;

    public GridTimer(GameGrid grid) {
        this.grid = grid;
        this.delay = 1000 / Settings.getInstance().getSpeedOfGame();

        task = new TimerTask() {
            @Override
            public void run() {
                if (!paused) {
                    currentTick++;
                    grid.step(currentTick);
                }
            }
        };
    }

    public void start() {
        if (!this.started) {
            this.scheduleAtFixedRate(this.task, 0, delay);
            this.started = true;
        } else {
            this.paused = false;
        }
    }

    public void pause() {
        this.paused = true;
    }

    public long getCurrentTick() {
        return currentTick;
    }
}
