package cz.zcu.kiv.uur.game;

import cz.zcu.kiv.uur.customComponents.CanvasPane;
import cz.zcu.kiv.uur.game.entities.FoodPoint;
import cz.zcu.kiv.uur.game.entities.Point;
import cz.zcu.kiv.uur.game.entities.Snake;
import cz.zcu.kiv.uur.game.drawer.GridDrawer;
import cz.zcu.kiv.uur.game.rating.Rating;
import cz.zcu.kiv.uur.game.rating.RatingList;
import cz.zcu.kiv.uur.game.timer.GridTimer;
import cz.zcu.kiv.uur.parsers.RatingsParser;
import cz.zcu.kiv.uur.settings.Settings;
import cz.zcu.kiv.uur.windows.MainWindow;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class GameGrid extends HBox {
    private CanvasPane canvasPane = new CanvasPane();
    private Settings settings = Settings.getInstance();

    private FoodPoint currentFood;
    private GridDrawer drawer = new GridDrawer(this);
    private GridTimer timer;

    private Map<KeyCode, Runnable> keyPressActions = new HashMap<KeyCode, Runnable>();

    List<Point> allPoints = generateAllPoints();
    List<Snake> snakes = new ArrayList<>();

    public GameGrid() {
        canvasPane.getCanvasWidthProperty().addListener(e -> drawer.drawGrid());
        canvasPane.getCanvasHeightProperty().addListener(e -> drawer.drawGrid());

        this.getChildren().add(canvasPane);
        this.setAlignment(Pos.CENTER);

        setDarkMode(false);

        this.widthProperty().addListener(e -> sizeChange());
        this.heightProperty().addListener(e -> sizeChange());

        MainWindow.getInstance().getScene().setOnKeyPressed(e -> handleKeyPress(e));
    }

    public void setDarkMode(boolean isDark) {
        if (isDark) {
            this.setBackground(new Background(new BackgroundFill(Color.web("0x000000"), CornerRadii.EMPTY, new Insets(0))));
            canvasPane.setBackground(Color.web("0x222222"));
            drawer.drawGrid();
        } else {
            this.setBackground(new Background(new BackgroundFill(Color.web("0xEBECF0"), CornerRadii.EMPTY, new Insets(0))));
            canvasPane.setBackground(Color.WHITE);
            drawer.drawGrid();
        }
    }

    public void addSnake(Snake snake) {
        this.snakes.add(snake);
    }

    public void setFood(FoodPoint point) {
        this.currentFood = point;
    }

    private List<Point> generateAllPoints() {
        List<Point> list = new ArrayList<>();
        for (int i = 0; i < settings.getAreaSize(); i++) {
            for (int j = 0; j < settings.getAreaSize(); j++) {
                list.add(new Point(i, j));
            }
        }

        return list;
    }

    public List<Point> getFreeSpaces() {
        List<Point> points = new ArrayList<>(allPoints);

        for (Snake snake : snakes) {
            points.removeAll(snake.getPoints());
        }

        if (currentFood != null) {
            points.remove(currentFood.getPoint());
        }

        return points;
    }

    public long getCurrentTick() {
        if (timer == null) {
            return 0;
        }
        return this.timer.getCurrentTick();
    }

    public CanvasPane getCanvasPane() {
        return this.canvasPane;
    }

    private void sizeChange() {
        double width = this.getWidth();
        double height = this.getHeight();

        double squareSize = Math.min(width, height);

        this.canvasPane.setPrefHeight(squareSize);
        this.canvasPane.setPrefWidth(squareSize);
    }

    public void newGame() {
        this.snakes.clear();
        MainWindow.getInstance().resetWindow();
        if (timer != null) {
            timer.cancel();
        }

        allPoints = generateAllPoints();

        timer = new GridTimer(this);

        canvasPane.requestFocus();

        newFood();

        for (int i = 0; i < settings.getNumberOfPlayers(); i++) {
            switch (i) {
                case 0:
                    addSnake(new Snake(0, this, settings.getPlayerSettings(i)));
                    break;
                case 1:
                    addSnake(new Snake(1, this, settings.getPlayerSettings(i)));
                    break;
                case 2:
                    addSnake(new Snake(-1, this, settings.getPlayerSettings(i)));
                    break;
                case 3:
                    addSnake(new Snake(2, this, settings.getPlayerSettings(i)));
                    break;
                case 4:
                    addSnake(new Snake(-2, this, settings.getPlayerSettings(i)));
                    break;
            }
        }

        this.updateTimeLabel();
        this.updateScoreLabel();
        drawer.drawGrid();
    }

    public void startTimer() {
        if (timer == null) {
            newGame();
        }
        timer.start();
        canvasPane.requestFocus();
    }

    public void pauseTimer() {
        timer.pause();
    }

    public void step(long currentTick) {
        for (Snake snake : snakes) {
            snake.move();
        }

        if (currentFood.checkTimerRunOut(currentTick)) {
            currentFood = new FoodPoint(this);
        }

        boolean atLeastOneAlive = false;
        for (Snake snake : snakes) {
            if (snake.isAlive()) {
                atLeastOneAlive = true;
                break;
            }
        }

        if (!atLeastOneAlive) {
            processSnakeRatings();
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    showEndGameDialog();
                }
            });

            timer.cancel();
        }

        updateTimeLabel();
        drawer.drawGrid();
    }

    public List<Snake> getSnakes() {
        return this.snakes;
    }

    public FoodPoint getFood() {
        return this.currentFood;
    }

    public void newFood() {
        this.currentFood = new FoodPoint(this);
    }

    private void showEndGameDialog() {
        currentFood = null;
        MainWindow.getInstance().resetWindow();
        drawer.drawGrid();

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Konec hry!");
        alert.setHeaderText("Konec hry!");
        alert.setContentText("Konečné výsledky:\n\n" + newTimeLabelText() + "\n\n" + newScoreLabelText(true));
        alert.showAndWait();
    }

    private void handleKeyPress(KeyEvent e) {
        Runnable r = keyPressActions.get(e.getCode());
        if (r != null) {
            r.run();
            e.consume();
        }
    }

    private String newScoreLabelText(boolean spaces) {
        String label = "";
        for (Snake snake : snakes) {
            label += snake.getName();
            label += ": ";
            label += snake.getScore();
            label += "b    ";
            if (spaces) label += "\n";
        }
        return label;
    }

    private void processSnakeRatings() {
        for (Snake snake : snakes) {
            Rating rating = new Rating(snake.getName(), snake.getTicks() / settings.getSpeedOfGame(), snake.getScore());
            RatingList.getInstance().addRating(rating);
        }

        try {
            RatingsParser.saveDefaultRatings();
        } catch (IOException e) {
            System.out.println("Nelze uložit soubor s výsledky!");
        }
    }

    public String newTimeLabelText() {
        int speed = settings.getSpeedOfGame();

        long seconds = this.getCurrentTick() / speed;
        long minutes = seconds / 60;

        seconds -= minutes * 60;

        String labelText = "Čas hry: ";
        if (minutes != 0) {
            labelText += minutes;
            labelText += "m ";
        }

        labelText += seconds;
        labelText += "s";

        return labelText;
    }

    public void unbindKey(KeyCode kc) {
        this.keyPressActions.remove(kc);
    }

    public void bindKey(KeyCode kc, Runnable r) {
        this.keyPressActions.put(kc, r);
    }

    public void updateScoreLabel() {
        Platform.runLater(() -> MainWindow.getInstance().setScoreLabel(newScoreLabelText(false)));
    }

    public void updateTimeLabel() {
        Platform.runLater(() -> MainWindow.getInstance().setTimeLabel(newTimeLabelText()));
    }
}
