package cz.zcu.kiv.uur.customComponents;

import cz.zcu.kiv.uur.game.rating.Rating;
import javafx.scene.control.TableCell;

public class DoubleCell extends TableCell<Rating, Double> {
    private int round = 2;

    public DoubleCell(int round) {
        this.round = round;
    }

    public void updateItem(Double value, boolean empty) {
        if (empty) {
            setText("");
        } else {
            setText(String.format("%." + round + "f", value).replace(',', '.'));
        }
    }
}
