package cz.zcu.kiv.uur.customComponents;

import cz.zcu.kiv.uur.game.rating.Rating;
import javafx.scene.control.TableCell;

public class TimeCell extends TableCell<Rating, Long> {
    public void updateItem(Long seconds, boolean empty) {
        if (empty) {
            setText("");
        } else {
            long minutes = seconds / 60;

            seconds -= minutes * 60;

            String timeText = "";
            if (minutes != 0) {
                timeText += minutes;
                timeText += "m ";
            }

            timeText += seconds;
            timeText += "s";

            setText(timeText);
        }
    }
}
