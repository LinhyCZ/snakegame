package cz.zcu.kiv.uur.customComponents;

import cz.zcu.kiv.uur.game.rating.Rating;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;

public class OrderCell extends TableCell<Rating, Integer> {
    private final Label label;

    public OrderCell() {
        label = new Label();
    }

    public void updateItem(Integer value, boolean empty) {
        super.updateItem(value, empty);
        if (empty) {
            setGraphic(null);
        } else {
            int poradi = getIndex() + 1;
            if (poradi == 1) {
                label.setTextFill(Color.GOLD);
                label.setFont(Font.font("Arial", FontWeight.BOLD, FontPosture.REGULAR, 15));
            } else if (poradi == 2) {
                label.setTextFill(Color.SILVER);
                label.setFont(Font.font("Arial", FontWeight.BOLD, FontPosture.REGULAR, 13));
            } else if (poradi == 3) {
                label.setTextFill(Color.web("0xCD7F32")); //Bronze
                label.setFont(Font.font("Arial", FontWeight.BOLD, FontPosture.REGULAR, 12));
            } else {
                label.setTextFill(Color.BLACK);
                label.setFont(Font.font("Arial", FontWeight.NORMAL, FontPosture.REGULAR, 11));
            }

            label.setText(poradi + "");

            setGraphic(label);
        }
    }
}
