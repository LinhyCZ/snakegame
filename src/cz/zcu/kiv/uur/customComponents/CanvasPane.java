package cz.zcu.kiv.uur.customComponents;

import javafx.beans.property.DoubleProperty;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

public class CanvasPane extends Pane {
    private final Canvas canvas;

    private Color background = Color.WHITE;

    public CanvasPane() {
        canvas = new Canvas();

        this.getChildren().add(canvas);

        canvas.heightProperty().bind(this.heightProperty());
        canvas.widthProperty().bind(this.widthProperty());

        this.setOnMouseClicked(event -> this.requestFocus());
    }

    public GraphicsContext getGraphicsContext2D() {
        return canvas.getGraphicsContext2D();
    }

    public DoubleProperty getCanvasWidthProperty() {
        return canvas.widthProperty();
    }

    public DoubleProperty getCanvasHeightProperty() {
        return canvas.heightProperty();
    }

    public double getCanvasSize() { return this.canvas.getHeight(); }

    public Color getBackgroundColor() {
        return background;
    }

    public void setBackground(Color background) {
        this.background = background;
    }
}
