Instalace:
Aplikaci není třeba instalovat, stačí pouze otevří soubor Snake.jar, pro spuštění je třeba Java alespoň verze 11




Ovládání:
Tlačítko "Nová hra" vyresetuje aktuální hru.
Tlačítko "Začít hrát!" spustí hru.
Tlačítko "Pauza" (nahradí tlačítko "Začít hrát!") pozastaví aktuální hru.
Tlačítko "Pokračovat" (nahradí tlačítko "Pauza") spustí pozastavenou hru.

V horní liště jsou tlačítka pro zobrazení nastavení a tabulky hodnocení

V pravé části je vypsán čas hry a skóre hráčů.

Po stisknutí tlačítka "Začít hrát!" se spustí hra - jednotliví hadi se ovládají pomocí kláves navolených v nastavení.
Potrava je reprezentována zelenými nebo žlutými kolečky v herním poli.
Žlutá potrava představuje bonusovou potravu - ta přidá hráči 5 bodů.
Herní pole není ohraničeno - pokud had dojede na kraj herního pole, pokračuje z druhé strany




Nastavení:
Rychlost hry - Rychlost, jakou se pohybují hadi (1 - nejpomalejší, 10 - nejrychlejší)
Velikost herního pole - Rozměr pole, ve kterém se pohybují hadi
Generovat bonusovou potravu - Pokud je zaškrtnuto, generuje bonusovou potravu
Pravděpodobnost bonusové potravy - Pravděpodobnost vygenerování bonusové potravy v procentech
Interval mizení potravy - Počet kroků, za které se vygeneruje nová potrava

Export nastavení - Lze vygenerovat soubor s nastavením, který lze importovat v jiné aplikaci
Importovat nastavení - načíst nastavení ze souboru




Tabulka výsledků:
Tabulka výsledků obsahuje výsledky všech hráčů seřazené podle největšího počtu bodů.
